class PredictedData < ActiveRecord::Base
    validates :product_name, uniqueness: true, presence: true
end
