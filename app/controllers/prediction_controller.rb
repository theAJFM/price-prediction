class PredictionController < ApplicationController
  def index
    @predictlist = PredictedData.all
  end
  def upload
  end
  def create
    uploaded_io = params[:data]
    File.open('upload.txt', 'w+') do |file|
      file.write(uploaded_io.read)
    end
    @uploadfile = PredictedData.create(product_name: params[:productname])
    filenamein = 'model' << @uploadfile.id.to_s << '/dataset.txt'
    @uploadfile.link = filenamein
    @uploadfile.save
    dropbox_connect(filenamein)
    redirect_to root_url
  end
  
  def item
    @curr_id = params[:id]
    @product = PredictedData.find_by(id:@curr_id)
    @predicted, @real = train(@curr_id)
  end
end
