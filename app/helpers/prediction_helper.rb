require 'ruby-fann'
require 'dropbox_sdk'
module PredictionHelper
    def dropbox_connect(filenamein)
        access_token = 'RpwCcAJjNPEAAAAAAAAC3gDprRMUUyFvVtIVBezFNYTFIYC-_XvhHCDPEw_eO5EZ'.strip
        
        client = DropboxClient.new(access_token)
        puts "linked account:", client.account_info().inspect
        filein = open("upload.txt")
        response = client.put_file(filenamein, filein)
        puts "uploaded:", response.inspect
    end
    def train_model
        train = RubyFann::TrainData.new(:inputs=>[[0.418736, 0.422329, 0.425937, 0.420355]], :desired_outputs=>[[0.398923]])
        fann = RubyFann::Standard.new(:num_inputs=>4, :hidden_neurons=>[4, 2, 1], :num_outputs=>1)
        fann.train_on_data(train, 10000, 1, 0.0001) 
        outputs = fann.run([0.371007, 0.369072, 0.359094, 0.369549])
        outputs
    end
    
    def train(id)
        temp = []
        data = []
        normDax = []
        input = []
        output = []
        inputTest = []
        outputTest = []
        predicted = []
        real = []
        it = 0
        access_token = 'RpwCcAJjNPEAAAAAAAAC3gDprRMUUyFvVtIVBezFNYTFIYC-_XvhHCDPEw_eO5EZ'.strip
        client = DropboxClient.new(access_token)
        puts "linked account:", client.account_info().inspect
        contents = client.get_file("model#{id}/dataset.txt")
        data = contents.split("\n")
        data.each{|i| i.chomp!}
        data.map!{|i| i.to_f}
        maxDax = data.max
        data.each do |x|
            calculate = (x/maxDax)*0.8+0.1
            normDax.push(calculate)
        end
        print normDax
        puts ""
        while it+3 < normDax.length-1 do
            if it > (normDax.length-1)/4 * 3
                inputTest << normDax[it, 4]
                it += 1
                outputTest << [normDax[it+3]]
            else
                input << normDax[it, 4]
                it += 1
                output << [normDax[it+3]]
            end
        end
        train = RubyFann::TrainData.new(:inputs=>input, :desired_outputs=>output)
        testdata = RubyFann::TrainData.new(:inputs=>inputTest, :desired_outputs=>outputTest)
        fann = RubyFann::Standard.new(:num_inputs=>4, :hidden_neurons=>[1, 2], :num_outputs=>1)
        fann.train_on_data(train, 20000, 100, 0.0001)
        puts fann.test_data(testdata)
        puts "sds"
        input.each do |i|
            predicted << fann.run(i)
        end
        predicted.map!{|i| ((i[0]-0.1)/0.8)*maxDax}
        real = data[4..data.length-1]
        print testdata
        puts ""
        return [predicted, real]
    end
    
end
