class CreatePredictedData < ActiveRecord::Migration
  def change
    create_table :predicted_data do |t|
      t.string :product_name
      t.string :link
      t.timestamps
    end
  end
end
